using System;
using UnityEngine;

public class FlyElement : MonoBehaviour
{
    private FlyData data;
    private bool hasData = false;
    private FlyProcessingData processingData;

    public void SetData(FlyData value)
    {
        data = value;
        hasData = true;
        processingData = new FlyProcessingData
        {
            CurrentIndex = 0,
            Target = data.Path[0],
        };
    }

    private bool FlyProcessing()
    {
        Vector3 currentPosition = transform.position;
        transform.position = Vector3.Lerp(currentPosition, processingData.Target, 20 * Time.deltaTime);
        return Vector2.Distance(transform.position, processingData.Target) <= 0.1f;
    }
    
    private void Update()
    {
        if (hasData == false)
        {
            return;
        }

        if (FlyProcessing())
        {
            int index = processingData.CurrentIndex + 1;
            Vector3 target = index <= data.Path.Length - 1 ? data.Path[index] : data.Path[data.Path.Length - 1];
            processingData = new FlyProcessingData
            {
                CurrentIndex = index,
                Target = target,
            };
        }

        if (processingData.CurrentIndex >= data.Path.Length)
        {
            data.OnComplete?.Invoke();
        }
    }
    
    private struct FlyProcessingData
    {
        public int CurrentIndex;
        public Vector3 Target;
    }
}

public struct FlyData
{
    public Vector3[] Path;
    public Action OnComplete;
}