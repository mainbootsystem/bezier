using System.Collections.Generic;
using UnityEngine;

public class Bezier
{
    private const int MAX_POINT = 10;
    private const int DEGREE_2 = 2;
    
    public static Vector3[] GetQuadraticBezier(Vector3 startPoint, Vector3 middlePoint, Vector3 endPoint)
    {
        List<Vector3> bezier = new List<Vector3>();
        bezier.Add(startPoint);
        int stepCount = MAX_POINT + 1;
        float maxPointToFloat = MAX_POINT;
        for (int i = 1; i < stepCount; i++)
        {
            float t = i / maxPointToFloat;
            Vector3 point = CalculateQuadraticBezier(startPoint, middlePoint, endPoint, t);
            bezier.Add(point);
        }

        return bezier.ToArray();
    }
    
    private static Vector3 CalculateQuadraticBezier(Vector3 startPoint, Vector3 middlePoint, Vector3 endPoint, float t)
    {
        float u = (1f - t);
        float t2 = Mathf.Pow(t, DEGREE_2);
        float u2 = Mathf.Pow(u, DEGREE_2);
        Vector3 bezierPoint = u2 * startPoint;
        bezierPoint += DEGREE_2 * u * t * middlePoint;
        bezierPoint += t2 * endPoint;
        return bezierPoint;
    }
}