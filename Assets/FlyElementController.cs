using UnityEngine;

public class FlyElementController : MonoBehaviour
{
    [SerializeField] private RectTransform container;
    [SerializeField] private FlyElement prefab;
    [SerializeField] private RectTransform target;
    [SerializeField] private RectTransform start;
    [SerializeField] private Vector2 offset;

    private Vector3 GetMiddlePoint(Vector3 a, Vector3 b)
    {
        return new Vector2((a.x + b.x) * 0.5f, (a.y + b.y) * 0.5f);
    }

    [ContextMenu("Start Fly")]
    private void PlayFly()
    {
        Vector3 middle = GetMiddlePoint(start.position, target.position);
        middle += (Vector3)offset;
        Vector3[] bezier = Bezier.GetQuadraticBezier(start.position, middle, target.position);
        FlyElement element = Instantiate(prefab, container);
        FlyData data = new FlyData
        {
            Path = bezier,
            OnComplete = CompleteHandler,
        };
        element.SetData(data);
    }

    private void CompleteHandler()
    {
        Debug.Log("Complete");
    }
    
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(start.position, 10f);
        Gizmos.DrawSphere(target.position, 10f);
        Vector3 middle = GetMiddlePoint(start.position, target.position);
        middle += (Vector3)offset;
        Gizmos.DrawSphere(middle, 10f);
        Vector3[] bezier = Bezier.GetQuadraticBezier(start.position, middle, target.position);
        Gizmos.color = Color.green;
        for (int i = 0; i < bezier.Length; i++)
        {
            if (i + 1 >= bezier.Length)
            {
                return;
            }
            
            Gizmos.DrawLine(bezier[i], bezier[i + 1]);
        }
    }
}